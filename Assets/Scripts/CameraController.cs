﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    public Transform Anchor;
    public Camera mainCamera;

    [Header("Mouse Rotation")]
    public float rotationSpeed = 0.003f;
    public float rotationDeceleration = 0.03f;
    public float minVerticalAngle = 30f;
    public float maxVerticalAngle = 100f;

    [Header("Mouse Zoom")]
    public float zoomSpeed = 0.5f;
    public float zoomDeceleration = 0.01f;
    public float minZoom = 30f;
    public float maxZoom = 80f;

    [Header("Refocus")]  // Refocus = set camera focus to selected car part
    public float refocusRotationSpeed = 5f;
    public float refocusZoomSpeed = 5f;

    [System.Serializable]
    public struct CameraSetup
    {
        public float yRotation;
        public float zRotation;
        public float fieldOfView;
    }
    public List<CameraSetup> cameraPositions = new List<CameraSetup>();

    [Header("Idle")]  // Start the camera rotation automatically after user being idle for some time
    public float idleTime = 60f;
    public float idleRotationSpeed = 5f;

    private Vector3 lastMousePosition = Vector3.zero;
    private Vector3 deltaMousePosition = Vector3.zero;
    private Vector3 deltaAngles = Vector3.zero;
    private float deltaZoom = 0;
    private bool isInteractingWithUI = false;
    private float idleCounter = 0;
    private bool inIdle = false;


    // Update is called once per frame
    void Update()
    {
        CheckIdle();
        HandleInput();
        RotateCamera();
        ZoomCamera();
    }

    void HandleInput()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            deltaZoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed / Time.deltaTime;
        }
        else
        {
            deltaZoom *= Mathf.Pow(zoomDeceleration, Time.deltaTime);  // Zoom inertia
            if (Mathf.Abs(deltaZoom) < 1)
                deltaZoom = 0;
        }

        if (Input.GetMouseButtonUp(0))
            isInteractingWithUI = false;

        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) // Check if the mouse is over UI
            {
                isInteractingWithUI = true;
                return;
            }

            lastMousePosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            if (isInteractingWithUI)
                return;

            deltaMousePosition = lastMousePosition - Input.mousePosition;
            deltaAngles = -deltaMousePosition * rotationSpeed / Time.deltaTime;
            lastMousePosition = Input.mousePosition;
        }
        else
        {
            deltaAngles *= Mathf.Pow(rotationDeceleration, Time.deltaTime); // Camera rotation inertia
            if (Vector3.Magnitude(deltaAngles) < 1)
                deltaAngles = Vector3.zero;
        }
    }

    void RotateCamera()
    {
        if (Vector3.Magnitude(deltaAngles) == 0)
            return;
        Vector3 directionTowardsAnchor = Anchor.transform.position - transform.position;
        RotateAroundAxis(Vector3.up, deltaAngles.x);
        RotateAroundAxis(Anchor.forward, deltaAngles.y);
    }

    void ZoomCamera()
    {
        if (deltaZoom == 0)
            return;

        // Zoom limits
        if (mainCamera.fieldOfView + deltaZoom > maxZoom)
            mainCamera.fieldOfView = maxZoom;
        else if (mainCamera.fieldOfView + deltaZoom < minZoom)
            mainCamera.fieldOfView = minZoom;
        else
            mainCamera.fieldOfView += deltaZoom;
    }

    void RotateAroundAxis(Vector3 axis, float angle)
    {
        Quaternion rotation = Quaternion.AngleAxis(angle, axis);
        Quaternion anchorRotation = Anchor.rotation;
        anchorRotation *= Quaternion.Inverse(anchorRotation) * rotation * anchorRotation;
        float verticalAngle = (anchorRotation.eulerAngles.z + 90) % 180;

        // Rotation limits
        if (verticalAngle < minVerticalAngle)
            Anchor.rotation.eulerAngles.Set(anchorRotation.eulerAngles.x, anchorRotation.eulerAngles.y, minVerticalAngle);
        else if (verticalAngle > maxVerticalAngle)
            Anchor.rotation.eulerAngles.Set(anchorRotation.eulerAngles.x, anchorRotation.eulerAngles.y, maxVerticalAngle);
        else
            Anchor.rotation = anchorRotation;
    }

    public void GoToCameraPosition(int index)
    {
        StopAllCoroutines();
        StartCoroutine(RotateToTarget(cameraPositions[index].yRotation, cameraPositions[index].zRotation));
        StartCoroutine(ZoomToTarget(cameraPositions[index].fieldOfView));
    }

    // Smoothly set camera zoom to target
    IEnumerator ZoomToTarget(float targetFieldOfView)
    {
        while (Mathf.Abs(mainCamera.fieldOfView - targetFieldOfView) > 0.1f) // Stop zooming when current and target values are almost equal
        {
            mainCamera.fieldOfView = Mathf.Lerp(mainCamera.fieldOfView, targetFieldOfView, refocusZoomSpeed * Time.deltaTime);
            yield return null;
        }
        mainCamera.fieldOfView = targetFieldOfView;
        yield return null;
    }

    // Smoothly set camera rotation to target
    IEnumerator RotateToTarget(float targetRotationY, float targetRotationZ)
    {
        while (Mathf.Abs((Anchor.transform.rotation.eulerAngles - Quaternion.Euler(0f, targetRotationY, targetRotationZ).eulerAngles).magnitude) > 1f) // Stop rotating when current and target values are almost equal
        {
            Anchor.transform.rotation = Quaternion.Slerp(Anchor.transform.rotation, Quaternion.Euler(0f, targetRotationY, targetRotationZ), 
                refocusRotationSpeed * Time.deltaTime);
            yield return null;
        }
        Anchor.transform.rotation = Quaternion.Euler(0f, targetRotationY, targetRotationZ);
        yield return null;
    }

    // Start the camera rotation automatically after user being idle for some time
    void CheckIdle()
    {
        idleCounter += Time.deltaTime;

        if (Input.GetAxis("Mouse ScrollWheel") != 0 || Input.GetMouseButton(0))
        {
            inIdle = false;
            idleCounter = 0;
        }

        if (idleCounter > idleTime)
        {
            if (!inIdle)
            {
                inIdle = true;
                StartCoroutine(ZoomToTarget(60));
            }
            else
            {
                Anchor.Rotate(new Vector3(0, -idleRotationSpeed * Time.deltaTime, 0), Space.World);
            }
        }
    }
}
