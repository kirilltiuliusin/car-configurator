﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using TMPro;

public class Screenshot : MonoBehaviour
{
    public Animator flashAnimator;
    public Animator statusbarAnimator;
    public GameObject uiScreen;
    public GameObject flashScreen;
    public GameObject statusbar;
    public TextMeshProUGUI statusText;

    // Update is called once per frame
    void Update()
    {
        // Hiding Flash screen after animation ended
        if (flashScreen.activeInHierarchy && flashAnimator.GetCurrentAnimatorStateInfo(0).IsName("End"))
        {
            StartCoroutine(CaptureScreenshot());
            flashScreen.SetActive(false);
        }
        // Hiding status bar after animation ended
        if (statusbar.activeInHierarchy && statusbarAnimator.GetCurrentAnimatorStateInfo(0).IsName("End"))
        {
            statusbar.SetActive(false);
        }
    }

    public void ScreenshotButtonClicked()
    {
        uiScreen.SetActive(false);
        flashScreen.SetActive(true);
        flashAnimator.Play("Flash");
    }

    IEnumerator CaptureScreenshot()
    {
        string directory = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "Screenshots" +
            Path.DirectorySeparatorChar;
        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);
        ScreenCapture.CaptureScreenshot(directory + "Screenshot " + System.DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png");
        yield return new WaitForSeconds(0.5f);
        uiScreen.SetActive(true);
        statusbar.SetActive(true);
        statusText.text = "Screenshot saved to " + directory;
        statusbarAnimator.Play("Statusbar Fade");
        yield return null;
    }
}
