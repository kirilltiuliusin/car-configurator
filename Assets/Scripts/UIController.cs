﻿using System.Collections;
using System.Collections.Generic;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{
    public CameraController cameraController;

    public SimpleScrollSnap uiScrollSnap;

    public TextMeshProUGUI panelsLabel;

    public GameObject colorPickerPanel;
    public GameObject partsPickerPanel;
    public GameObject roofScoopPicker;
    public GameObject spoilerPicker;
    public GameObject wheelPicker;
    public GameObject clearancePanel;

    public List<string> panelLabels = new List<string>();


    private SimpleScrollSnap roofScoopScrollSnap;
    private SimpleScrollSnap spoilerScrollSnap;
    private SimpleScrollSnap wheelScrollSnap;

    private SimpleScrollSnap selectedScrollSnap;

    // Start is called before the first frame update
    void Start()
    {
        roofScoopScrollSnap = roofScoopPicker.GetComponent<SimpleScrollSnap>();
        spoilerScrollSnap = spoilerPicker.GetComponent<SimpleScrollSnap>();
        wheelScrollSnap = wheelPicker.GetComponent<SimpleScrollSnap>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    public void OnPanelSelected()
    {
        cameraController.GoToCameraPosition(uiScrollSnap.TargetPanel);
        panelsLabel.text = panelLabels[uiScrollSnap.TargetPanel];
        switch (uiScrollSnap.TargetPanel)
        {
            case 3: // Car Body option
                selectedScrollSnap = null;
                HideAllPickers();
                colorPickerPanel.SetActive(true); 
                break;
            case 2: // Spoiler option
                selectedScrollSnap = spoilerScrollSnap;
                HideAllPickers();
                partsPickerPanel.SetActive(true);  
                spoilerPicker.SetActive(true);
                break;
            case 1: // Roof Scoop option
                selectedScrollSnap = roofScoopScrollSnap;
                HideAllPickers();
                partsPickerPanel.SetActive(true);
                roofScoopPicker.SetActive(true);
                break;
            case 0: // Wheels option
                selectedScrollSnap = wheelScrollSnap;
                HideAllPickers();
                partsPickerPanel.SetActive(true);
                wheelPicker.SetActive(true);
                break;
        }
    }

    void HideAllPickers()
    {
        colorPickerPanel.SetActive(false);
        partsPickerPanel.SetActive(false);
        spoilerPicker.SetActive(false);
        roofScoopPicker.SetActive(false);
        wheelPicker.SetActive(false);
    }

    public void ClearanceButtonClicked()
    {
        clearancePanel.SetActive(!clearancePanel.activeSelf);
    }

    public void NextButtonClicked()
    {
        if (selectedScrollSnap)
            selectedScrollSnap.GoToNextPanel();
    }

    public void PreviousButtonClicked()
    {
        if (selectedScrollSnap)
            selectedScrollSnap.GoToPreviousPanel();
    }

    public int GetSelectedPartIndex()
    {
        return selectedScrollSnap ? selectedScrollSnap.TargetPanel : 0;
    }
}
