﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DanielLochner.Assets.SimpleScrollSnap;

public class CarModification : MonoBehaviour
{
    public UIController uiController;

    [Header("Car Colors")]
    public Material carBodyMaterial;
    public FlexibleColorPicker colorPicker;
    public float metallicValue = 0.5f;

    [Header("Clearance")]
    public Transform carBody;
    public Slider clearanceSlider;

    [Header("Parts")]
    public List<GameObject> roofScoops;
    public List<GameObject> spoilers;
    public List<GameObject> wheels;

    [Header("Car Lights")]
    public Material lightsOff;
    public Material frontlightsOn;
    public Material backlightsOn;
    public MeshRenderer frontlights;
    public MeshRenderer backlights;
    public GameObject spotlights;


    private Color selectedColor;

    // Start is called before the first frame update
    void Start()
    {
        carBodyMaterial.SetFloat("_Metallic", 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (selectedColor != colorPicker.color)
        {
            selectedColor = colorPicker.color;
            carBodyMaterial.color = selectedColor;
        }
    }

    public void MetallicToggleChanged(bool isOn)
    {
        if (isOn)
        {
            carBodyMaterial.SetFloat("_Metallic", metallicValue);
        }
        else
        {
            carBodyMaterial.SetFloat("_Metallic", 0);
        }
    }

    public void WheelSelected()
    {
        DisableAllPartsInList(wheels);
        wheels[uiController.GetSelectedPartIndex()].SetActive(true);
    }

    public void RoofScoopSelected()
    {
        DisableAllPartsInList(roofScoops);
        if (uiController.GetSelectedPartIndex() != 0)
            roofScoops[uiController.GetSelectedPartIndex() - 1].SetActive(true);
    }

    public void SpoilerSelected()
    {
        DisableAllPartsInList(spoilers);
        if (uiController.GetSelectedPartIndex() != 0)
            spoilers[uiController.GetSelectedPartIndex() - 1].SetActive(true);
    }

    public void DisableAllPartsInList(List<GameObject> list)
    {
        foreach (GameObject gameObject in list)
        {
            gameObject.SetActive(false);
        }
    }

    public void ClearanceSliderChanged()
    {
        carBody.localPosition = new Vector3(0, clearanceSlider.value, 0);
    }

    public void ToggleLights()
    {
        if (spotlights.activeInHierarchy)
        {
            frontlights.material = lightsOff;
            backlights.material = lightsOff;
            spotlights.SetActive(false);
        }
        else
        {
            frontlights.material = frontlightsOn;
            backlights.material = backlightsOn;
            spotlights.SetActive(true);
        }
    }
}
